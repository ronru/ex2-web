package Java;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * The type Read from file, charge on reading from file and create list of url's
 */
public class ReadPollFile {
    /**
     * The Exit unsuccessful.
     */

    private Integer listSize; // size of url list
    private String fileName; //file name
    private ArrayList<String> answerList = new ArrayList<>(); //list of answer's
    private BufferedReader reader = null;


    /**
     * Instantiates a new Read from file.
     *
     * @param fn the file name
     */
    public ReadPollFile(String fn) throws PollNotValidException {
        this.fileName = fn;
        try {
            reader = new BufferedReader(new FileReader(fileName , StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new PollNotValidException("File Not Found");
        } finally {
            if (reader != null) {
                execute();
            }
        }
    }

    /**
     * Execute is responsible of reading from the file and create url list that save the url that read's from the file
     * in the function we throw IOException if reading is failed and print the message
     */
    public void execute() {

        String currentLine;
        try {
            while ((currentLine = reader.readLine()) != null)
                setAnswerStrings(currentLine);
            listSize(answerList.size());
        }catch (IOException e)
        {
            System.err.println(e.getMessage());
        }
    }

    /**
     * Gets url strings.
     *
     * @return the answer strings
     */
    public ArrayList<String> getAnswerList() {
        return answerList;
    }

    /**
     * Sets url strings.
     *
     * @param urlStrings the url strings
     */
    public void setAnswerStrings(String urlStrings) {
        this.answerList.add(urlStrings);
    }

    /**
     * Set size file.
     *
     * @param size the size of the file
     */
    public void listSize(int size){
        listSize = size;
    }

    /**
     * Gets size file.
     *
     * @return the size file
     */
    public Integer getSizeFile() {
        return listSize;
    }
}