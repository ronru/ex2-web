package Java;

/**
 * The Poll not valid exception.
 * the poll does not meet threshold conditions
 */
public class PollNotValidException extends Exception {


    /**
     * Instantiates a new Poll not valid exception.
     * without message
     */
    public PollNotValidException() { super();}


    /**
     * Instantiates a new Poll not valid exception.
     * with error message
     * @param errorMsg the error message
     */
    public PollNotValidException(String errorMsg) { super(errorMsg);}

}

