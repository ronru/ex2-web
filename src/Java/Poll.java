package Java;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class in charge on the Poll data, the data keeps in HaseMap
 * <String - for answers , Int - for quantity of votes>
 */
public class Poll {
    //for initial the counter
    private static final int INITIAL_VALUE = 0;
    private static final int MIN_DATA_VALUE = 3;
    //hold the question
    private String pollQuestion;
    //hold poll data
    private HashMap<String, Integer> pollData = null;


    /**
     * Instantiates a new Poll, With answers and counter to count the voters.
     *
     * @param pollList the poll list answers
     * @throws NullPointerException  the pointer is null
     * @throws PollNotValidException the poll n * the poll does not meet threshold conditions
     */
    public Poll(ArrayList<String> pollList) throws NullPointerException, PollNotValidException {
        if (pollFormIsValid(pollList)) {
            pollData = new HashMap<>();
            setPollQuestion(pollList.get(0));
            for (int i = 1; i < pollList.size(); i++) {
                pollData.put(pollList.get(i), INITIAL_VALUE);
            }
        }

    }

    private boolean pollFormIsValid(ArrayList<String> pollList) throws NullPointerException, PollNotValidException {

        if (pollList != null) {
            if (pollList.size() < MIN_DATA_VALUE) {
                System.out.println("not min val");
                throw  new PollNotValidException();
            }
        } else
            System.out.println("NullPtr");
        return true;
    }


    /**
     * Gets poll quantity voters.
     *
     * @param answer the answer - key value
     * @return the poll quantity voters - the value
     */
    public Integer getPollQuantityVoters(String answer) {
        return pollData.get(answer);
    }

    /**
     * Gets poll data obj.
     *
     * @return the poll data obj
     */
    public HashMap<String, Integer> getPollDataObj() {
        return pollData;
    }


    /**
     * Gets poll question.
     *
     * @return the poll question
     */
    public String getPollQuestion() {
        return pollQuestion;
    }

    /**
     * Sets poll question.
     *
     * @param pollQuestion the poll question
     */
    public void setPollQuestion(String pollQuestion) {
        this.pollQuestion = pollQuestion;
    }

    public synchronized void  updateResults(String questionPressed) {
        pollData.put(questionPressed,pollData.get(questionPressed)+1);
    }
}
