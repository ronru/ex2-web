package Servlets;

import Java.Poll;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This servlet show the poll result
 */
@WebServlet(name = "ShowPollResult", urlPatterns = "/ShowPollResult")
public class ShowPollResult extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext context = getServletContext();
        Poll pollMng = (Poll)context.getAttribute("PollMng");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();
        RequestDispatcher rd = request.getRequestDispatcher("/pollResult.html");
        rd.include(request,response);

        printHtmlResult(out , pollMng);

        out.close();

    }

    private void printHtmlResult(PrintWriter out, Poll pollMng) {
        out.println("<HTML><HEAD><LINK rel='stylesheet’ type='text/css' href='CSS/buttons.css'></HEAD><BODY>");
        out.println("<h1>תוצאות הסקר:</h1>");
        for (String i : pollMng.getPollDataObj().keySet()) {
            out.print("<li>" + i + " --> " + " כמות מצביעים: " + pollMng.getPollDataObj().get(i) + "</li>");
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
