package Servlets;
import Java.Poll;
import Java.PollNotValidException;
import Java.ReadPollFile;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;



/**
 * The type Main.
 */
@WebServlet(name = "Main" , urlPatterns = "/Main" ,initParams = @WebInitParam (name = "pollFile" , value = "poll.txt"))

public class Main extends HttpServlet {
public Poll pollMng  = null ;
public ServletContext context = null;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
     String filePath = getServletContext().getRealPath("/"+ getInitParameter("pollFile"));
        try {
            ReadPollFile reader = new ReadPollFile(filePath);
            pollMng = new Poll(reader.getAnswerList());
        }
        catch(PollNotValidException e) {
            throw new ServletException();
        }


    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        context = getServletContext();
        synchronized (context) { //the data need to be safe when we enter it to the global memory

            context.setAttribute("PollMng", pollMng);
        }
        response.sendRedirect("/main.html");
    }

    /**
     * File not found.
     */
    protected void fileNotFound() {

    }
}

