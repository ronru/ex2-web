package Servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "UserManagement", urlPatterns = "/UserManagement")
public class UserManagement extends HttpServlet {

    private static final String COOKIE_EXISTS = "true";
    private static final String SET_COOKIE = "true";
    private static final String NEW_USER = "UserName";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (userIsVoted(request, response))
            response.sendRedirect("/main.html");

        RequestDispatcher rd = request.getRequestDispatcher("ShowPollQuestions");
        rd.forward(request, response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public boolean userIsVoted(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            Cookie cookie = new Cookie(NEW_USER, SET_COOKIE);
            cookie.setMaxAge(360);
            response.addCookie(cookie);
            return false;
        } else {

            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getValue() == COOKIE_EXISTS) {
                    PrintWriter out = response.getWriter();
                    out.println("user exits !!");

                }
            }
            return true;
        }
    }
}