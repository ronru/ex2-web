package Servlets;

import Java.Poll;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ShowPollQuestions" , urlPatterns = "/ShowPollQuestions")
public class ShowPollQuestions extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ServletContext context = getServletContext();
        Poll pollMng = (Poll)context.getAttribute("PollMng");
        response.setContentType("text/html; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");

        PrintWriter out = response.getWriter();
        RequestDispatcher rd = request.getRequestDispatcher("/pollQuestions.html");
        rd.include(request,response);
        printHtmlQuestions(out , pollMng);

        out.close();
    }
    private void printHtmlQuestions(PrintWriter out, Poll pollMng) {
        out.println("<html><head><link rel='stylesheet’ type='text/css' href=CSS/buttons.css></head><body>");

        out.println("<h1>סקר</h1>");
        out.println("<h2>" + pollMng.getPollQuestion() +"</h2>");
        out.println("<form action=/pollInfoManagement method=post");

        for (String i : pollMng.getPollDataObj().keySet()) {
            out.println("<br><input type=radio name=question value=" + i + ">" + i +"</br>");
        }
        out.println("<br><input  type=submit value=בחר ></br></form>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
