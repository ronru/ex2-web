package Servlets;

import Java.Poll;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "pollInfoManagement" , urlPatterns = "/pollInfoManagement")
public class pollInfoManagement extends HttpServlet {


    private String questionPressed;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

            ServletContext context = getServletContext();
            questionPressed = request.getParameter("question");
            Poll pollMng = (Poll) context.getAttribute("PollMng");
            pollMng.updateResults(questionPressed);
            RequestDispatcher rd = request.getRequestDispatcher("ShowPollResult");
            rd.forward(request, response);

    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
